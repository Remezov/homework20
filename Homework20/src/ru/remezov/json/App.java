package ru.remezov.json;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;

public class App {
    public static void main(String[] args) {
        try {
            URL url = new URL("https://dog.ceo/api/breeds/image/random ");
            try (InputStream is = url.openStream();
                 Reader reader = new InputStreamReader(is)){
                ObjectMapper objectMapper = new ObjectMapper();
                Dog dog = objectMapper.readValue(reader, Dog.class);
                System.out.println(dog.toString());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }
}
