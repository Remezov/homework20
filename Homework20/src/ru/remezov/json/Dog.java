package ru.remezov.json;

public class Dog {
    private String message;
    private String status;

    public Dog() {}

    public Dog(String message, String status) {
        this.message = message;
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return String.format("собака [ сообщение:%s, статус:%s]",
                message, status);
    }
}
